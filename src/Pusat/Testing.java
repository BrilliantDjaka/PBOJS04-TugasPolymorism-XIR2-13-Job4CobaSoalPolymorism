package Pusat;
import java.util.Arrays;
import java.util.Scanner;


public class Testing {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Jumlah Bangun : ");
        int testcase = sc.nextInt();
        Bangun[] bangun = new Bangun[testcase];
        
        for (int i = 0; i < testcase; i++) {
            System.out.println("Bangun ke : " + (i+1));
            pilihBangun();
            int choice = sc.nextInt();
            try{
            if(choice == 1){ // segitiga
                System.out.println("Bangun Datar Segitiga");
                double tempSisi;
                double tempTinggi;
                System.out.print("Masukkan Sisi : ");
                tempSisi =sc.nextDouble();
                System.out.print("Masukkan Tinggi : ");
                tempTinggi = sc.nextDouble();
                bangun[i] = new Segitiga(tempSisi, tempTinggi);
            }
            else if(choice == 2){//Persegi
                System.out.println("Bangun Datar Persegi");
                double tempSisi;
                System.out.print("Masukkan Sisi : ");
                tempSisi =sc.nextDouble();
                bangun[i] = new Persegi(tempSisi);
            
            }
            else if(choice == 3){ // Trapesium
                System.out.println("Bangun Datar Trapesium");
                double tempSisiKanan,tempSisiKiri,tempSisiAtas,tempSisiBawah;
                double tempTinggi;
                System.out.print("Masukkan Sisi Kanan : ");
                tempSisiKanan =sc.nextDouble();
                System.out.print("Masukkan Sisi Kiri : ");
                tempSisiKiri =sc.nextDouble();
                System.out.print("Masukkan Sisi Atas : ");
                tempSisiAtas =sc.nextDouble();
                System.out.print("Masukkan Sisi Bawah : ");
                tempSisiBawah =sc.nextDouble();
                System.out.print("Masukkan Tinggi : ");
                tempTinggi = sc.nextDouble();
                bangun[i] = new Trapesium(tempSisiKiri,tempSisiAtas,tempSisiBawah,tempSisiKanan,tempTinggi);
                
            
            }
            else if(choice == 4){//lingkaran
                System.out.println("Bangun Datar Lingkaran");
                double tempJariJari;
                System.out.print("Masukkan Jari-Jari : ");
                tempJariJari =sc.nextDouble();
                bangun[i] = new Lingkaran(tempJariJari);
            
            }
            
        }catch(Exception e){
            e.getMessage();
            continue;
        }
        }
        for (int i = 0; i < testcase; i++) {
            if(bangun[i] instanceof Persegi){
                cetakLuasDankeliling(((Persegi)bangun[i]),i,((Persegi)bangun[i]));
                
            }
            
            else if(bangun[i] instanceof Segitiga){
                
                
                cetakLuasDankeliling(((Segitiga)bangun[i]),i,((Segitiga)bangun[i]));
                
            }
            else if(bangun[i] instanceof Trapesium){
                cetakLuasDankeliling(((Trapesium)bangun[i]),i,((Trapesium)bangun[i]));
            }
            else if(bangun[i] instanceof Lingkaran){
                cetakLuasDankeliling(((Lingkaran)bangun[i]),i,((Lingkaran)bangun[i]));
            }
            
    }
        System.out.println("=====================================");
    }
    
    static void pilihBangun(){
        System.out.println("Silahkan pilih bangun datar : ");
        System.out.println("1.Segitiga \n2.Persegi \n3.Trapesium \n4.Lingkaran ");
        System.out.print("Pilih Nomor : ");
        
    }
    
    static void cetakLuasDankeliling(ICetak a,int i,Bangun b){
        System.out.println("=====================================");
        System.out.println("Bangun ke "+(i+1));
        if(b instanceof Persegi) System.out.println("sisi = "+((Persegi)b).sisi);
        else if(b instanceof Segitiga) System.out.println("sisi = "+((Segitiga)b).sisi+"\nSisi Miring :"+((Segitiga)b).miring+"\nTinggi : "+((Segitiga)b).tinggi);
        else if(b instanceof Trapesium) System.out.println("sisi Atas = "+((Trapesium)b).sisiAtas+"\nsisi Bawah = "+((Trapesium)b).sisiBawah+"\nsisi Kiri = "+((Trapesium)b).sisiKiri+"\nsisi Kanan = "+((Trapesium)b).sisiKanan+"\nTinggi = "+((Trapesium)b).tinggi);
        else if(b instanceof Lingkaran) System.out.println("\nJari-Jari "+((Lingkaran)b).jariJari+"\nDiameter = "+(((Lingkaran)b).jariJari*2));
        a.cetakKeliling();
        a.cetakLuas();
        
    }
    
}
