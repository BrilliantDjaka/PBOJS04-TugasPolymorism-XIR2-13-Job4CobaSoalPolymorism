/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pusat;

/**
 *
 * @author ASUS
 */
public class Segitiga extends Bangun implements ICetak{
    double sisi;
    double tinggi;
    double miring;
    public Segitiga(double sisi,double tinggi){
        this.sisi = sisi;
        this.tinggi = tinggi;
        this.miring = Math.sqrt(Math.pow(sisi, 2)+Math.pow(tinggi, 2));
    }
    @Override
    double luas() {
        return sisi*tinggi/2;
    }
    
    @Override
    double keliling() {
        return sisi+miring+tinggi;
    }

    @Override
    public void cetakLuas() {
        System.out.println("Luas Segitiga : "+luas());
    }

    @Override
    public void cetakKeliling() {
        System.out.println("Keliling Segitiga : "+keliling());
    }
    
}
