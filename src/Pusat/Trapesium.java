/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pusat;

/**
 *
 * @author ASUS
 */
public class Trapesium extends Bangun implements ICetak{
    double sisiKiri,sisiAtas,sisiBawah,sisiKanan,tinggi;

    public Trapesium(double kiri,double atas,double bawah,double kanan,double t){
        sisiKiri = kiri;
        sisiKanan = kanan;
        sisiAtas = atas;
        sisiBawah = bawah;
        tinggi = t;
    }
    @Override
    double luas() {
        return (sisiAtas+sisiBawah)*tinggi/2;
    }

    @Override
    double keliling() {
        return  sisiKiri+sisiBawah+sisiKanan+sisiAtas;
    }

     @Override
    public void cetakLuas() {
        System.out.println("Luas Trapesium : "+luas());
    }

    @Override
    public void cetakKeliling() {
        System.out.println("Keliling Trapesium : "+keliling());
    }
    
}
