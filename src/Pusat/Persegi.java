/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pusat;

/**
 *
 * @author ASUS
 */
public class Persegi extends Bangun implements ICetak{
    double sisi;
    public Persegi(double sisi){
        this.sisi = sisi;
    }
    @Override
    double luas() {
        return sisi*sisi;
    }

    @Override
    double keliling() {
        return sisi*4;
    }

     @Override
    public void cetakLuas() {
        System.out.println("Luas Persegi : "+luas());
    }

    @Override
    public void cetakKeliling() {
        System.out.println("Keliling Persegi : "+keliling());
    }
    
}
