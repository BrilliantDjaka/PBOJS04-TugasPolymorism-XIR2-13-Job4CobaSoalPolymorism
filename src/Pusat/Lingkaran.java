/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pusat;

/**
 *
 * @author ASUS
 */
public class Lingkaran extends Bangun implements ICetak{
    private final double pi = 3.14;
    double jariJari ;
    
    public Lingkaran(double a){
        jariJari = a;
    }
    @Override
    double luas() {
        return pi*jariJari*jariJari;
    }

    @Override
    double keliling() {
        return jariJari*2*pi;
    }

    @Override
    public void cetakLuas() {
        System.out.println("Luas Lingkaran : "+luas());
    }

    @Override
    public void cetakKeliling() {
        System.out.println("Keliling Lingkaran : "+keliling());
    }
    
}
